function matchesWonPerYear(matches) {
    let wins = {}
    for (let match of matches) {
      if (wins[match.season]) {
        wins[match.season][match.winner] += 1
      }
  
     else {
        let team = {}
        for (let item of matches) {
          team[item.winner] = 1
        }
        wins[match.season] = team
      }
    }

    
    return wins
  
}
module.exports=matchesWonPerYear
