function highestDismissal(deliveries){
    let res={}

    for(let delivery of deliveries){
        if((delivery.player_dismissed!=="")& (delivery.dismissal_kind!=="run out")){
            let bowler=delivery.bowler
            let batsman=delivery.batsman
            let str=batsman+"-"+bowler

           if(res[str]){
                res[str]+=1
            }else{
                res[str] =1
               
            }
        }
    }

    //return res
    let array=Object.entries(res)
//return array
let sort=array.sort((a,b)=>b[1]-a[1])
//return sort
let final=sort.slice(0,1)
return Object.fromEntries(final)
}
module.exports=highestDismissal